//
//  ViewController.swift
//  Challenge-03
//
//  Created by Tatang Sulaeman on 25/03/22.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var photoUi: UIImageView!
    @IBOutlet weak var fieldOne: UITextField!
    @IBOutlet weak var fieldTwo: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    var image = ["photoOne", "photoTwo", "photoThree"]
    var someVar = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        photoUi.image = UIImage(named: "photoOne")
        resultLabel.text = String(0)
        
        // Do any additional setup after loading the view.
    
    }
    
    
    @IBAction func pressButton(_ sender: Any) {
        photoUi.image = UIImage(named: image[Int(arc4random_uniform(3))])
        guard let num1 = Int(fieldOne.text!),
              let num2 = Int(fieldTwo.text!) else {
                  return
              }
        let sum = num1 + num2
        resultLabel.text = String(sum)
    }
}

